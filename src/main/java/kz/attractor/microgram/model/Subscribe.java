package kz.attractor.microgram.model;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Subscribe {

    @Id
    private String id;
    private String publicUserId;
    private String subUserId;
    private LocalDateTime date;

    public Subscribe(String publicUserId, String subUserId, LocalDateTime date) {
        this.publicUserId = publicUserId;
        this.subUserId = subUserId;
        this.date = date;
    }
}