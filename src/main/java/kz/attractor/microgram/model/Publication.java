package kz.attractor.microgram.model;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import kz.attractor.microgram.util.Generator;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Data
@Builder
public class Publication {
    @Id
    private String id;
    private String description;
    private LocalDateTime date;
    private int likes;
    private String image;
    @DBRef
    private User user;

    @DBRef
    @Builder.Default
    private Image postImage;

    public static Publication makeNew(User user) {
        return builder()
                .user(user)
                .description(Generator.makeDescription())
                .date(LocalDateTime.now())
                .image("https://ih1.redbubble.net/image.638903182.0784/raf,750x1000,075,t,434847:965054cba4.u1.jpg")
                .build();
    }

}