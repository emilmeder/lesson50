package kz.attractor.microgram.model;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import java.time.LocalDate;
import java.util.List;

@Document
@Data
public class Like {
    @Id
    private String id;
    private String userId;
    private String pubId;
    private LocalDate date;

    public Like(String userId, String pubId, LocalDate date) {
        this.userId = userId;
        this.pubId = pubId;
        this.date = date;
    }
}