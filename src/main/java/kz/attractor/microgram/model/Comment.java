package kz.attractor.microgram.model;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Random;

import kz.attractor.microgram.util.Generator;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;


@Document
@Data
@Builder
public class Comment {
    @Id
    private String id;
    private String text;
    private LocalDateTime date;
    private int likes;
    @DBRef
    private User user;
    @DBRef
    private Publication post;

    public static Comment makeNew(User user, Publication post) {
        return builder()
                .text(Generator.makeDescription())
                .date(LocalDateTime.now())
                .user(user)
                .post(post)
                .build();
    }


}