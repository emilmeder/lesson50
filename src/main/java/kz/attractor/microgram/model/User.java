package kz.attractor.microgram.model;
import kz.attractor.microgram.util.Generator;
import kz.attractor.microgram.util.SecurityConfig;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.ArrayList;
import java.util.List;


@Document
@Data
@Builder
public class User implements UserDetails {
    @Builder.Default
    @Id
    private String id;
    private String name;
    private String email;
    private String password;

    @DBRef
    private List<User> subscribers;

    @DBRef
    private List<User> subscriptions;


    public static User makeNew() {
        return builder()
        .email(Generator.makeEmail())
        .name(Generator.makeName())
        .password(SecurityConfig.encoder().encode(Generator.makePassword()))
        .build();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.of(new SimpleGrantedAuthority("FULL"));
    }

    @Override
    public String getUsername() {
        return getEmail();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public void setPassword(String password) {

        this.password = SecurityConfig.encoder().encode(password);
    }
}
