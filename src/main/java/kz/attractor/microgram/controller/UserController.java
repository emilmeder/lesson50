package kz.attractor.microgram.controller;
import kz.attractor.microgram.Repository.*;
import kz.attractor.microgram.annotations.ApiPageable;
import kz.attractor.microgram.dto.*;
import kz.attractor.microgram.model.*;
import kz.attractor.microgram.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import java.io.*;


@Controller
@RequestMapping("/user")
public class UserController {
    private final UserService us;
    private final PublicationService ps;
    private final ImageService is;
    private final PublicationRepository pr;
    private final CommentRepository cr;
    private final UserRepository ur;

    public UserController(UserService us, PublicationService ps, ImageService is, PublicationRepository pr, CommentRepository cr, UserRepository ur) {
        this.us = us;
        this.ps = ps;
        this.is = is;
        this.pr = pr;
        this.cr = cr;
        this.ur = ur;
    }

    @ApiPageable
    @GetMapping
    public Slice<UserDto> findUsers(@ApiIgnore Pageable pageable) {
        return us.findUsers(pageable);
    }

    @GetMapping("/byName/{name}")
    public UserDto getUserByName(@PathVariable("name") String name) {
        return us.findByName(name);
    }

    @GetMapping("/byEmail/{email}")
    public UserDto getUserByEmail(@PathVariable("email") String email) {
        return us.findByEmail(email);
    }


    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public UserDto addNewUser(@RequestBody UserDto userData) {
        return us.addUser(userData);
    }

    @PostMapping(path ="/add_post" , consumes = MediaType.APPLICATION_JSON_VALUE)
    public PublicationDto addPost(@RequestBody PublicationDto postData) {
        return ps.addPost(postData);
    }

    @GetMapping("/posts")
    public String showPost(){
        return "index";

    }
    @PostMapping(path = "/saveImage", produces = MediaType.APPLICATION_JSON_VALUE)
    public String saveImage(@RequestParam("image") MultipartFile file,
                            Authentication authentication) throws IOException {
        File image = new File("D:/images/"+file.getOriginalFilename());
        FileOutputStream os = new FileOutputStream(image);
        os.write(file.getBytes());
        os.close();
        var user = (User)authentication.getPrincipal();
        var post = Publication.makeNew(user);
        post.setImage("http://localhost:9889/user/image/" + image.getName());
        pr.save(post);

        return "redirect:/user/posts";
    }
    @PostMapping(path = "/saveComment",produces = MediaType.APPLICATION_JSON_VALUE)
    public String saveComment(@RequestParam("comment") String comment, @RequestParam("postId") String id, Authentication authentication){
        User user = (User)authentication.getPrincipal();
        var newPublication = Publication.makeNew(user);
        newPublication.setId("qqw");
        pr.save(newPublication);
        var newComment = Comment.makeNew(user, pr.findById(id).get());
        newComment.setText(comment);
        cr.save(newComment);

        return "index";
    }

    @GetMapping("/image/{name}")
    @ResponseBody
    public ResponseEntity<byte[]> getImage(@PathVariable("name")String name){
        String path = "D:\\images";
        try {
            InputStream is = new FileInputStream(new File(path) + "/" + name);
            return ResponseEntity
            .ok()
            .contentType(name.toLowerCase().contains(".jpg") ? MediaType.IMAGE_JPEG : MediaType.IMAGE_PNG)
            .body(StreamUtils.copyToByteArray(is));
        }
        catch (Exception e){
            InputStream is = null;
            try {
                is = new FileInputStream(new File(path) + "/" + name);
                return ResponseEntity
                .ok()
                .contentType(name.toLowerCase().contains(".jpg") ? MediaType.IMAGE_JPEG : MediaType.IMAGE_PNG)
                .body(StreamUtils.copyToByteArray(is));
            }
            catch (IOException ex){
                ex.printStackTrace();
            }
            e.printStackTrace();
        }
        return null;
    }
    @GetMapping(value = "/image2/{name}", produces = MediaType.IMAGE_JPEG_VALUE)
    @ResponseBody
    public byte[] getImage2(@PathVariable("name") String name) {
        String path = "D:\\images";
        try {
            InputStream is = new FileInputStream(new File(path) + "/" + name);
            return StreamUtils.copyToByteArray(is);
        }
        catch (Exception e) {
            InputStream is = null;
            try {
                is = new FileInputStream(new File(path) + "/" + name);
                return StreamUtils.copyToByteArray(is);
            }
            catch (IOException ex) {
                ex.printStackTrace();
            }
            e.printStackTrace();
        }
        return null;
    }


    @PostMapping("/register")
    public User createUser(@RequestParam("email")String email,@RequestParam("name")String name,@RequestParam("login")String login,@RequestParam("password")String password){
        var user = User.makeNew();
        user.setEmail(email);
        user.setPassword(password);
        user.setName(login);
        user.setName(name);
        ur.save(user);
        return user;
    }
}
