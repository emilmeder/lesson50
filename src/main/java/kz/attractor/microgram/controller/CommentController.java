package kz.attractor.microgram.controller;
import com.sun.source.tree.BreakTree;
import kz.attractor.microgram.Repository.*;
import kz.attractor.microgram.annotations.ApiPageable;
import kz.attractor.microgram.model.*;
import kz.attractor.microgram.dto.*;
import kz.attractor.microgram.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;


@RestController
@RequestMapping("/comments")
public class CommentController {
    CommentService cs;

    public CommentController(CommentService cs) {
        this.cs = cs;

    }

    @ApiPageable
    @GetMapping
    public Slice<CommentDto> findAllComments(@ApiIgnore Pageable pageable) {
        return cs.findComments(pageable);
    }

}

