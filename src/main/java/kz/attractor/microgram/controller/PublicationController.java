package kz.attractor.microgram.controller;
import com.sun.source.tree.BreakTree;
import kz.attractor.microgram.Repository.*;
import kz.attractor.microgram.annotations.ApiPageable;
import kz.attractor.microgram.model.*;
import kz.attractor.microgram.dto.*;
import kz.attractor.microgram.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

@RestController
@RequestMapping("/posts")
public class PublicationController {

    PublicationService ps;

    public PublicationController(PublicationService ps) {

        this.ps = ps;
    }

    @ApiPageable
    @GetMapping("/byUserId/{user_id}")
    public Slice<PublicationDto> getAllUsersPost(@ApiIgnore Pageable pageable, @PathVariable("user_id") String user_id) {
        return ps.findAllUserPost(pageable, user_id);
    }

    @ApiPageable
    @GetMapping
    public Slice<PublicationDto> findPosts(@ApiIgnore Pageable pageable) {

        return ps.findPosts(pageable);
    }
}

