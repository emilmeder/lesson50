package kz.attractor.microgram.util;
import kz.attractor.microgram.model.*;
import kz.attractor.microgram.Repository.*;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

@Configuration
public class DatabasePreloader {
    private static final Random r = new Random();


    @Bean
    CommandLineRunner initDatabase(UserRepository ur, PublicationRepository pr, CommentRepository cr, ImageRepository ir) {
        return (args) -> {
            ur.deleteAll();
            pr.deleteAll();
            cr.deleteAll();
            ir.deleteAll();

            var test = User.makeNew();
            test.setEmail("qqw@gmail.com");
            test.setPassword("qqw");
            ur.save(test);

            List<User> users = Stream.generate(User::makeNew).limit(10).collect(toList());
            ur.saveAll(users);
            System.out.println(users);

            List<Publication> posts = Stream.generate(() -> Publication.makeNew(users.get(r.nextInt(users.size())))).limit(10).collect(toList());
            pr.saveAll(posts);
            System.out.println(posts);

            List<Comment> comments = Stream.generate(() -> Comment.makeNew(users.get(r.nextInt(users.size())), posts.get(r.nextInt(posts.size())))).limit(10).collect(toList());
            cr.saveAll(comments);
            System.out.println(comments);
        };
    }
}