package kz.attractor.microgram.Repository;

import kz.attractor.microgram.model.Subscribe;
import kz.attractor.microgram.model.User;
import org.springframework.data.repository.CrudRepository;

public interface SubscribeRepository extends CrudRepository<Subscribe, String> {



}
