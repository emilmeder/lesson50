package kz.attractor.microgram.Repository;

import kz.attractor.microgram.model.*;

import org.springframework.data.repository.CrudRepository;

public interface ImageRepository extends CrudRepository<Image, String> {
        Image findPublicationImageById(String id);
    }

