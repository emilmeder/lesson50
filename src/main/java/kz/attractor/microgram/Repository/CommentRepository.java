package kz.attractor.microgram.Repository;

import kz.attractor.microgram.model.Comment;
import kz.attractor.microgram.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface CommentRepository extends PagingAndSortingRepository<Comment, String> {

    public void deleteCommentById(String id);
    Comment findCommentByText(String text);
}
