package kz.attractor.microgram.Repository;

import kz.attractor.microgram.model.Like;
import kz.attractor.microgram.model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface LikeRepository extends CrudRepository<Like, String> {
//    boolean existsByUser_Id(String id);
//
//    List<Like> findAllByUser_Id(String id);
}

