package kz.attractor.microgram.Repository;

import kz.attractor.microgram.model.Publication;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface PublicationRepository extends PagingAndSortingRepository<Publication, String> {

    @Query("{'id': {'$ne': '?0'}}")

    Page<Publication> findAllByUser_Id(String id, Pageable pageable);

    List<Publication> findAllByUserId(String id);
}
