package kz.attractor.microgram.dto;
import kz.attractor.microgram.model.*;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class PublicationDto {

    public static PublicationDto from(Publication post) {
        var postPhotoId = post.getPostImage() == null
                ? "-no-image-id"
                : post.getPostImage().getId();

        return builder()
                .id(post.getId())
                .imageId(postPhotoId)
                .description(post.getDescription())
                .date(post.getDate())
                .userId(post.getUser().getId())
                .likes(post.getLikes())
                .image(post.getImage())
                .postImage(post.getPostImage())
                .build();
    }

    private String id;
    private String imageId;
    private String description;
    private LocalDateTime date;
    private String userId;
    private int likes;
    private String image;
    private Image postImage;


}


