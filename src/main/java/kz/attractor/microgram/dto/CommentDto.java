package kz.attractor.microgram.dto;
import kz.attractor.microgram.model.*;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class CommentDto {

    public static CommentDto from(Comment comment) {
        return builder()
                .id(comment.getId())
                .text(comment.getText())
                .date(comment.getDate())
                .userId(comment.getUser().getId())
                .likes(comment.getLikes())
                .postId(comment.getPost().getId())
                .name(comment.getUser().getName())
                .build();
    }

    private String id;
    private String text;
    private LocalDateTime date;
    private String name;
    private String userId;
    private String postId;
    private int likes;


}
