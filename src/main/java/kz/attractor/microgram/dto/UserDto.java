package kz.attractor.microgram.dto;
import kz.attractor.microgram.model.*;
import lombok.*;
import java.util.List;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class UserDto {

    public static UserDto from(User user) {
        return builder()
                .id(user.getId())
                .name(user.getName())
                .email(user.getEmail())
                .password(user.getPassword())
                .subscribers(user.getSubscribers())
                .subscriptions(user.getSubscriptions())
                .build();
    }

    private String id;
    private String name;
    private String email;
    private String password;
    private List<User>subscribers;
    private List<User>subscriptions;

}
