package kz.attractor.microgram.service;

import kz.attractor.microgram.Repository.UserRepository;
import kz.attractor.microgram.model.User;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class UserAuthService implements UserDetailsService {
    private final UserRepository ur;

    @Override
    public User loadUserByUsername(String userMail) throws UsernameNotFoundException {
        Optional<User> user = ur.findByEmail(userMail);
//        if (user.isPresent())
        if (user.isEmpty()) {
            throw new UsernameNotFoundException("User does not exit");
        }
        return user.get();
    }
}

