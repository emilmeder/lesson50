package kz.attractor.microgram.service;


import kz.attractor.microgram.Repository.ImageRepository;
import kz.attractor.microgram.dto.ImageDto;
import kz.attractor.microgram.exception.ResourceNotFoundException;
import kz.attractor.microgram.model.Image;
import org.bson.types.Binary;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
public class ImageService {
    private final ImageRepository ir;

    public ImageService(ImageRepository ir) {
        this.ir = ir;
    }

    public Image addImage(MultipartFile file) {
        byte[] data = new byte[0];
        try {
            data = file.getBytes();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (data.length == 0) {
            // TODO return no content or something or throw exception
            //  which will be processed on controller layer
        }

        Binary bData = new Binary(data);
        Image postImage = Image.builder().postData(bData).build();

        ir.save(postImage);

        return postImage;

    }

    public Resource getById(String imageId) {
        Image postImage = ir.findById(imageId)
                .orElseThrow(() -> new ResourceNotFoundException("Post Image with " + imageId + " doesn't exists!"));
        return new ByteArrayResource(postImage.getPostData().getData());
    }
}
