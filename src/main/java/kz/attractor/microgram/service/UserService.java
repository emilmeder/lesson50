package kz.attractor.microgram.service;
import kz.attractor.microgram.Repository.CommentRepository;
import kz.attractor.microgram.Repository.*;
import kz.attractor.microgram.exception.ResourceNotFoundException;
import kz.attractor.microgram.model.*;
import kz.attractor.microgram.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Service;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {
    private UserRepository ur;
    private PublicationRepository pr;
    private LikeRepository lr;

    @Autowired
    public UserService(UserRepository ur, PublicationRepository pr, LikeRepository lr) {
        this.ur = ur;
        this.pr = pr;
        this.lr = lr;
    }

    public List<Publication> getUsersPublications(String id) {
        return pr.findAllByUserId(id);
    }

    public Slice<UserDto> findUsers(Pageable pageable) {
        var slice = ur.findAll(pageable);
        return slice.map(UserDto::from);
    }

    public UserDto findByName(String userName) {
        var user = ur.findByName(userName).orElseThrow(() -> new ResourceNotFoundException("Doesn't exist: " + userName));
        return UserDto.from(user);
    }

    public UserDto findByEmail(String email) {
        var user = ur.findByEmail(email).orElseThrow(() -> new ResourceNotFoundException("Doesn't exist: " + email));
        return UserDto.from(user);
    }


    public UserDto addUser(UserDto userDTO) {
        User user = User.builder()
        .id(userDTO.getId())
        .name(userDTO.getName())
        .email(userDTO.getEmail())
        .password(userDTO.getPassword())
        .build();
        ur.save(user);

        return UserDto.from(user);
    }



}
