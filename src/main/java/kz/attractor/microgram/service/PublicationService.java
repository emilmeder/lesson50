package kz.attractor.microgram.service;

import kz.attractor.microgram.Repository.CommentRepository;
import kz.attractor.microgram.Repository.*;
import kz.attractor.microgram.exception.ResourceNotFoundException;
import kz.attractor.microgram.model.*;
import kz.attractor.microgram.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Service;
import springfox.documentation.annotations.ApiIgnore;

@Service
public class PublicationService {
    final
    PublicationRepository pr;
    final
    UserRepository ur;
    final
    ImageRepository ir;


    @Autowired
    public PublicationService(PublicationRepository pr, UserRepository ur, ImageRepository ir) {
        this.pr = pr;
        this.ur = ur;
        this.ir = ir;
    }


    public Slice<PublicationDto> findAllUserPost(Pageable pageable, String userId) {
        var slice = pr.findAllByUser_Id(userId, pageable);
        return slice.map(PublicationDto::from);
    }

    public PublicationDto addPost(PublicationDto postData) {
        var postImage = ir.findById(postData.getImageId())
                .orElseThrow(() -> new ResourceNotFoundException("Post Image with " + postData.getImageId() + " doesn't exists!"));
//        //TODO one user can have only one review per movie. add check
        var user = ur.findById(postData.getUserId()).get();
        var post = Publication.builder()
                .id(postData.getId())
                .postImage(postImage)
                .user(user)
                .date(postData.getDate())
                .description(postData.getDescription())
                .build();
        pr.save(post);

        // TODO recalculate rating after save. Update movie document

        return PublicationDto.from(post);
    }

    public Slice<PublicationDto> findPosts(Pageable pageable) {
        var slice = pr.findAll(pageable);
        return slice.map(PublicationDto::from);
    }
}