package kz.attractor.microgram.service;
import kz.attractor.microgram.Repository.CommentRepository;
import kz.attractor.microgram.Repository.*;
import kz.attractor.microgram.model.*;
import kz.attractor.microgram.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Service;
import springfox.documentation.annotations.ApiIgnore;

@Service
public class CommentService {
    private final CommentRepository cr;
    private final UserRepository ur;
    private final PublicationRepository pr;


    public CommentService(CommentRepository cr, UserRepository ur, PublicationRepository pr) {
        this.cr = cr;
        this.ur = ur;
        this.pr = pr;
    }

    public Slice<CommentDto> findComments(Pageable pageable) {
        var slice = cr.findAll(pageable);
        return slice.map(CommentDto::from);
    }

//    public CommentDTO addComment(CommentDTO commentData) {
//        var user = userRepository.findById(commentData.getUserId()).get();
//        var post = postRepository.findById(commentData.getPostId()).get();
//        var comment = Comment.builder()
//                .text(commentData.getText())
//                .date(commentData.getDate())
//                .user(user)
//                .post(post)
//                .build();
//        commentRepository.save(comment);
//
//        // TODO recalculate rating after save. Update movie document
//
//        return CommentDTO.from(comment);
//    }
}