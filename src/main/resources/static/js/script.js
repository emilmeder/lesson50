'use strict';

function  showSplashScreen(){
    let one = document.getElementById("page-splash");
    // let two = document.getElementById('body');

    one.hidden = false;
    // two.body.classList.add("no-scroll");
    document.body.classList.add("no-scroll");
}
function  hideSplashScreen(){
    let one = document.getElementById("page-splash");
    // let two = document.getElementById('body');

    one.hidden = true;
    // two.body.classList.remove("no-scroll");
    document.body.classList.remove("no-scroll");
}


function createCommentElement(comment) {
    let div = document.createElement('div');
    const html = `
        <a href=# class=muted>${comment.name}</a>
         <p>${comment.text}</p>;`;

    div.innerHTML+=html;
    div.classList.add("py-3", "pl-3");
    div.id = "comment" + comment.id
    return div;
}


// function createCommentElement(comment) {
//     let elemental = document.createElement(`div`);
//     elemental.innerHTML = "<a href=# class=muted>" + comment.user + "</a>";
//     elemental.innerHTML = `<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Chooh chooh mot$erF*c#er Ipsum ad est cumque nulla voluptatem enim voluptas minima illum quis! Voluptatibus dolorem minus tempore aliquid corrupti nesciunt, obcaecati fuga natus officiis.</p>`;
//
//     let newAttribute = document.createAttribute("class");
//     newAttribute.value = "py-2 pl-3";
//
//     elemental.setAttributeNode(newAttribute);
//     return elemental;
// }

function createPostElement(y) {
    let div = document.createElement('div');
    const html = `
    <div >
         <img id="image${y.id}" class="d-block w-100 image" src="${y.image} " alt="Post image">
    </div>
  <div class="px-4 py-3" id="post${y.id}">
    <div class="d-flex justify-content-around">
       <span class="h1 mx-2 muted">
          <i class="far fa-heart"  id="like${y.id}"></i>
        </span>
        <span class="h1 mx-2 muted">
          <i class="far fa-comment" id="comment${y.id}"></i>
        </span>
        <span class="mx-auto"></span>
        <span class="h1 mx-2 muted" id="save${y.id}">
          <i class="far fa-bookmark"></i>
        </span>
    </div>
  <hr>
  <div>
    <p>${y.description}</p>
    </div>
  <hr>
  <div id = "commentArea${y.id}" class="comment-container">
  </div>
   <div id="commentForm${y.id}">
						<form id="form" name="form" enctype="multipart/form-data" action="/post" method="post">
							<input type="text" placeholder=" " name="comment">
							<button onclick="f2()" type="button" id="newComment">create comment</button>
						</form>
          
   </div>
  </div>
  `;
    div.innerHTML += html;
    div.classList.add("card", "my-3");
    return div;
}

// function createPostElement(x) {
//     let div = document.createElement('div');
//     const html = `
//     <div >
//          <img id="image${x.id}" class="d-block w-100 img" src="${x.image} " alt="Post image">
//     </div>
//   <div class="px-4 py-3" id="post${x.id}">
//     <div class="d-flex justify-content-around">
//        <span class="h1 mx-2 muted">
//           <i class="far fa-heart"  id="like${x.id}"></i>
//         </span>
//         <span class="h1 mx-2 muted">
//           <i class="far fa-comment" id="comment${x.id}"></i>
//         </span>
//         <span class="mx-auto"></span>
//         <span class="h1 mx-2 muted" id="book${x.id}">
//           <i class="far fa-bookmark"></i>
//         </span>
//     </div>
//   <hr>
//   <div>
//     </div>
//   <hr>
//   <div id = "commentField${x.id}" class="comment-container">
//   </div>
//    <div id="commentForm${x.id}">
//
//    </div>
//   </div>
//   `;
//     div.innerHTML += html;
//     div.classList.add("card", "my-3");
//     return div;
// }

function addPost(x) {
    document.getElementsByClassName('posts-container')[0].append(x);
}
$(document).on('click', '.fa-heart', function (e) {
    e.preventDefault();
    let thisLike = e.currentTarget;
    thisLike.classList.contains('text-danger')
        ? thisLike.classList.remove('text-danger', 'fas')
        : thisLike.classList.add('text-danger', 'fas');
});


$(document).on('click', '.fa-bookmark', function (e) {
    e.preventDefault();
    let thisLike = e.currentTarget;
    thisLike.classList.contains('fas')
    ? thisLike.classList.remove('fas')
    : thisLike.classList.add('fas');
});

$(document).on('dblclick', '.image', function (e) {
    let thisImage = e.currentTarget;
    let imageId = thisImage.id.substr(5 - thisImage.id.length);
    let thisLike = document.getElementById("like"+imageId);
    thisLike.classList.contains('text-danger')
        ? thisLike.classList.remove('text-danger', 'fas')
        : thisLike.classList.add('text-danger', 'fas');
});


// let img = document.getElementsByClassName("img")[0];
// img.addEventListener('dblclick',function(){
//     let heartImg = like.children[0];
//     if(heartImg.classList.contains("text-danger")){
//
//         heartImg.classList.remove("text-danger","fas");
//     }
//     else{
//         heartImg.classList.add("text-danger","fas");
//     }
// });

const url = '/user/saveImage';

document.addEventListener('DOMContentLoaded', init);

function init() {
    document.getElementById('btnSubmit').addEventListener('click', upload);
}

$(document).on('click', '.fa-comment', function (e) {
    let thisComment = e.currentTarget;
    let commentId = thisComment.id.substr(7 - thisComment.id.length);

    let comment = document.getElementById("commentForm"+commentId);
    if (comment.style.visibility === "hidden") {
        comment.style.visibility = "visible";
    }

    else {
        comment.style.visibility = "hidden";
    }
});

async function getPosts() {
    const response = await fetch('http://localhost:9889/posts');

    if (response.ok) {
        let json = await response.json();
        console.log(json);
        return json;

    }
    else {
        alert("Error: " + response.status);
    }
}

async function f() {
    let posts = await getPosts();
    posts.content.forEach(elem =>{
        !document.getElementById(`post${elem.id}`)&&addPost(createPostElement(elem));
    })
}

function upload(ev) {
    ev.preventDefault();
    let h = new Headers();
    h.append('Accept', 'application/json');
    let formData = new FormData();
    let myFile = document.getElementById('image').files[0];
    formData.append('image', myFile, myFile.name);
    let request = new Request(url, {
    method: 'POST',
    headers: h,
    body: formData
    });

    fetch(request)

        .catch((err) => {
            console.log('ERROR:', err.message);
        });
}

document.addEventListener('DOMContentLoaded', up);

function up() {
    document.getElementById('commentButton').addEventListener('click', uploadComment);
}

let commentId = "";
$(document).on('click', '.commentForm', function (e) {
    let thisComment = e.currentTarget;
    commentId = thisComment.id.substr(11 - thisComment.id.length);
});

const url1 = "/user/saveComment"
function uploadComment(e) {
    e.preventDefault();
    let h = new Headers();
    h.append('Accept', 'application/json');
    let formData = new FormData();
    let text = document.getElementById("newComment"+commentId).value;
    let id = document.getElementById("postId" + commentId).value;
    formData.append('comment', text);
    formData.append('postId', id);
    let request = new Request(url1, {
        method: 'POST',
        headers: h,
        body: formData
    });
    fetch(request)
        .catch((err) => {
            console.log('ERROR:', err.message);
        });
}

async function getComments() {
    const response = await fetch('http://localhost:9889/comments');
    if (response.ok) {
    let json = await response.json();
    console.log(json);
    return json;
    }
    else {
        alert("Error: " + response.status);
    }
}

async function f2() {
    let comments = await getComments();
    for(let i = 0; i<comments.content.length;i++){
    if(comments.content[i].postId===commentId){
        if(document.getElementById("comment"+comments.content[i].id)===null) {
            let x = document.getElementById("commentArea" + commentId);
            x.append(createCommentElement(comments.content[i]));
        }else{
            return console.error();
        }
    }else{
        return console.error();
        }
    }
}



