'use strict';

const loginForm = document.getElementById('login-form');

loginForm.addEventListener('submit', LoginHandler);

    function LoginHandler(e) {
        e.preventDefault();
        const form = e.target;
        const userFormData = new FormData(form);
        const user = Object.fromEntries(userFormData);
        saveUser(user);
        authorized('http://localhost:9889/login')
        .catch(console.error)
    }

    function rest() {
        const userAsJSON = localStorage.getItem('user');
        const user = JSON.parse(userAsJSON);
        return user;

    }

    function authorized(url, options) {
        const settings = options || {}
        return fetch(url, updateOptions(settings));
    }

    function saveUser(user) {
        const userAsJSON = JSON.stringify(user)
        localStorage.setItem('user', userAsJSON);
    }

// function rest() {
//     const userAsJSON = localStorage.getItem('user');
//     const user = JSON.parse(userAsJSON);
//     return user;
//
// }
    function updateOptions(options) {
        const update = { ...options
        };
        update.mode = 'cors';
        update.headers = { ... options.headers };
        update.headers['Content-Type'] = 'application/json';
        const user = rest();
        if(user) {
            update.headers['Authorization'] = 'Basic ' +
                btoa(user.username + ':' + user.password);
        }
        return update;
    }