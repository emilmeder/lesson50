'use strict';
const baseUrl = 'http://localhost:9889';

const registrationForm = document.getElementById('registration-form');
registrationForm.addEventListener('submit', onRegisterHandler);

    function onRegisterHandler(e) {
        e.preventDefault();
        const form = e.target;
        const data = new FormData(form);
        makeNew(data).catch(console.error);
    }


    async function makeNew(userJSON) {
        const register = {
            method: 'POST',
            body: userJSON
    };

    const response = await fetch('http://localhost:9889/user/register',
        register);

    const responseData = await response.json();


    console.log(responseData);
    window.location.href = 'http://localhost:9889/login';
}